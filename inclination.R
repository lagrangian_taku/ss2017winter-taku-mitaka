
#%
#%
#%   inclination
#%
#%

## setup for filename ###############################################
iiis <- c('00','10','30','40','50','60','70','80','90')
aaas <- c('00','05','0998')
fkrs <- c('f','k','r')
n <- 4
#####################################################################

## i vector : preparation ##
i_partial <- matrix(0,9,1)    # 4 factors
counter <- 0
for (i in c(10,20,40,50,60,70,80,90,100)){               ###
  counter <- counter + 1

  i_partial[counter,1] <- 1.0*(i-10)
}
i_mini <- rbind(i_partial, i_partial, i_partial) # 12 factors
i_vec <- rbind(i_mini,i_mini,i_mini)   # 36 factors
# check dim(i_vec)


############################
b1_ov_row <- matrix(0,1,36)
b2_ov_row <- matrix(0,1,30)
b_ov <- matrix(0,81,(36+30))  # matrix(0,dim(i_vec)[1],(36+30))
############################

## B matrix
counter <- 0

for (aaa in aaas){
  for (fkr in fkrs){
    for (iii in iiis){
      counter <- counter + 1
      filename <- sprintf("testaa%s%s%s.dat", aaa, fkr, iii)
      dat <- read.table(filename)

      # Here, 'for-loop' reaps each ccolumn's factors
      for (j in c(1:36)){
        b1_ov_row[1,j] <- dat[j,4]
      }
      for (k in c(7:36)){
        b2_ov_row[1,(k-6)] <- dat[k,5]
      }

      b_ov_row <- cbind(b1_ov_row,b2_ov_row)
      b_ov[counter,] <- b_ov_row

    }
  }
}

dim(b_ov) #dimension of B_matrix: 'row' * 'column'

image.plot(b_ov,col = rgb(cols(0:99/99)/255))
#grid(nx=9,ny=9,col=1,lwd=0.7)
#abline(h=0.672)

tB <- t(b_ov)
tBB_ov <- tB%*%b_ov
det(tBB_ov)              # approx 0
solve(tBB_ov)            # but solvable
sol <- solve(tBB_ov)     # (tBB)^-1
tbiv <- tB %*% i_vec
sol %*% tB %*% i_vec
xx <- sol %*% tB %*% i_vec
i_kai <- b_ov %*% xx
