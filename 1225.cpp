
#include <stdio.h>
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <math.h>

template<typename Num_type>

bool Getdata(std::string filename, std::vector<std::vector<Num_type> >& data){

    std::ifstream reading_file;
    reading_file.open(filename,std::ios::in);

    std::string reading_line_buffer;

    std::cout << "reading " << filename << "..." << std::endl;

    //1行目を空読み(適用していたデータの都合上)
    //1行目から読みたい場合は下の一行をコメントアウトする
    //getline(reading_file,reading_line_buffer);

    Num_type num;
    char comma;

    while(!reading_file.eof()){
	std::vector<Num_type> temp_data;
	getline(reading_file,reading_line_buffer);
	std::istringstream is(reading_line_buffer);
	while(!is.eof()){
	    is >> num >> comma;
	    temp_data.push_back(num);
	}
	data.push_back(temp_data);
    }

    return true;
}


//画像のBessel級数展開
//besselJ_mapは取り込んだ画像にBessel級数展開を施す
//double *p[]とした

double besselJ_map(int N_grid, int nu, int k, double  th0, double dx,double  dy, double* p[])
{

    double x_cen, y_cen;
    
    //読み込み先2次元vector 
    std::vector<std::vector<double> > data;

    //ベッセル関数の零点のリストを読み込む
    Getdata("bessel0-2.csv",data);

    //表示
    for(int i=0 ;i < data.size() ;i++){
    for(int j=0 ;j < data[i].size() ;j++){
        std::cout << data[i][j] << " ";
        double jj[data.size()][data[i].size()];
        jj[i][j] = data[i][j];

    }
    std::cout << std::endl;
    }
    std::cout << std::endl;

    
        x_cen = N_grid/2 + 1 + dx;
        y_cen = N_grid/2 + 1 + dy;

        double z[N_grid-1][N_grid-1];

        for (int i = 0; i < N_grid; ++i)
        {   
            for (int j = 0; j < N_grid; ++j)
            {
                double xx = (1.0 * i - x_cen) / N_grid * 2;
                double yy = (1.0 * j - y_cen) / N_grid * 2;
                double theta = atan2(yy,xx);
                double rr = sqrt(pow(xx,2) + pow(yy,2));
                if ( rr <= 1.0 )
                {
                    double ss = rr * data[k][nu+1];

                    double tt = jn(nu, ss);
                    z[i][j] = tt * cos(nu * theta - th0);
                    //z[i][j] = _jn(nu, rr * data[k][nu+1] ) * cos(nu * theta -th0);
                }

            }
            int j = 0;
            p[i] = &z[i][j];
        }
        
    return 0;
}

int main(){

    int N_grid = 128;
    double *p[N_grid];
    besselJ_map(N_grid, 1, 1, 0.0, 0.0,0.0, p);
    std::cout << p << std::endl;
    std::cout << *(p[0]+1) << std::endl;
    
    return 0;
}