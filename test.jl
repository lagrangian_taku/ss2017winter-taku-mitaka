
using FITSIO
using PyPlot

function bessel_map(N_grid, nu, k, th0, dx, dy)
# (N_grid::Int64, nu::Int64, k::Int64, th0::Float64, dx::Float64, dy::Float64)
# (N_grid, nu, k, th0, dx, dy)
	x_cen = N_grid/2 + 1 + dx
	y_cen = N_grid/2 + 1 + dy 

	XX = Array{Float64}(1,N_grid)
	YY = Array{Float64}(1,N_grid)
	
	for i in 1 : N_grid 
		XX[i] = ( i - x_cen ) / N_grid *2
		YY[i] = ( i - y_cen ) / N_grid *2
	end

	z = zeros{Float64}(N_grid,N_grid)

	for ix in 1:N_grid
		for iy in 1:N_grid
			xx = (1.0 * ix - x_cen)/N_grid *2
			yy = (1.0 * iy - y_cen)/N_grid *2
			theta = atan2(yy, xx)
			rr = sqrt(xx^2.0 + yy^2.0)

			if rr <= 1.0

				z[ix,iy] = besselj(nu, rr*j0[k,nu+1]) * cos(nu*theta - th0)
			end
		end
	end

	return z 
end





bessel_file = "bessel0.csv"
totalrows = countlines(open( bessel_file, "r" ))
columns = 10 + 1

# ベッセル係数の格納配列
dat = zeros(Float64,totalrows+1,columns) 


open( bessel_file, "r" ) do fp
    cnt = 0
   # (ファイルの行数,2)のゼロ行列
    for line in eachline( fp ) # 行ごとに読み込む
    cnt += 1
        line = rstrip(line, '\n') # 改行消し。無くても大丈夫。
        u = split(line, ',') # コンマ区切りで分割
        uu = zeros(Float64,1, columns)
        for i in 1:columns
        	uu[i] = parse(Float64,u[i])
        	dat[cnt,i] = uu[i]
        end
    end
    #println(dat) # 書き出してみる。
    #print(size(dat))
end


### 0 position for Jn(x)
j0 = dat
N_nu_max = size(j0)[2] - 5
N_nu_max = Int64(N_nu_max)
N_k_max  = size(j0)[1] - 9
N_k_max  = Int64(N_k_max)
print(size(j0))

# manual dimension setting
#   N_nu_max = 3
#	N_k_max  = 3

f = FITS("fi.fits")

println(size(f[1]))
# size(f[1])
# ndims(f[1])

# fitsをそのまま読み込んできたので配列f[1]は４次元．
# 従って，128*128の画像の部分のみ刈り取ってきて，画像の次元を２に落とす．
ff = read(f[1],:,:,1,1)
println(size(ff)) # (128,128)と表示される
println(size(ff)[1]) # 128と表示される

N_grid = size(ff)[1]  
N_grid = Int64(N_grid)

#PyPlot.pause(imshow(ff))

readdir("FITS-data/.")
# println(readdir("FITS-data"))

filename  = readdir("FITS-data")
dirnm     = "FITS-data/"

for fn in filename
   #
	imagefile = "im_" * fn
	print(imagefile)
	println(fn)
   #

    fits_image = FITS(dirnm*fn)
    testimage  = read(fits_image[1],:,:,1,1)
    println(size(testimage))

    z_img   = testimage
    coeff_c = zeros(Float64,N_nu_max,N_k_max) 
    coeff_s = zeros(Float64,N_nu_max,N_k_max) 

   # 
   	dx  =0.0
   	dy  =0.0
   	th0 = 0.0


   	for nu in 0:(N_nu_max-1)
   		for k in 1:N_k_max
   			z = bessel_map(N_grid,nu,k,0,dx,dy)

   			coeff_c[nu+1,k] = sum(z*z_img)/sum(z*z)
   			

   			if nu != 0
   				z = bessel_map(N_grid,nu,k,pi/2.0,dx,dy)
   				coeff_s[nu+1,k] <- sum(z*z_img)/sum(z*z)
   			end

   		end
   	end

   #

   	z_mod = zeros(Float64,N_grid,N_grid)

   	for nu in 0:(N_nu_max-1) 
   		for k in 1:N_k_max

   			z     = bessel_map(N_grid,nu,k,0,dx,dy)
   			z_mod = z_mod + coeff_c[nu+1,k]*z 
   			z     = bessel_map(N_grid,nu,k,pi/2.0,dx,dy)
   			z_mod = z_mod + coeff_s[nu+1,k]*z

   		end
   	end


end


